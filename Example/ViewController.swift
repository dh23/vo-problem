//
//  ViewController.swift
//  Example
//
//  Created by Dawid Herman on 16/09/2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.yellow]
            navBarAppearance.backgroundColor = UIColor.purple
            navigationController?.navigationBar.standardAppearance = navBarAppearance
            navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        }
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "My title"
        let button = UIBarButtonItem(title: "T", style: .plain, target: self, action: #selector(buttonTapped))
        button.accessibilityLabel = "my button"
        navigationItem.rightBarButtonItem = button
    }

    @objc func buttonTapped() {
        print("tapped")
    }
}

